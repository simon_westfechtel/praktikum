/**
 * Funktional.java
 *
 * @Author Simon Westfechtel
 * @Version 1.1 (previous version: 1.0)
 */

package pack;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Funktional {

    /**
     * Main:
     * <ul>
     *     <li>Read xml file</li>
     *     <li>Create token list</li>
     *     <li>Parse token list into album list</li>
     *     <li>Print album list to console</li>
     *     <li>Catch possible exceptions</li>
     * </ul>
     * @param args ..
     * @throws IOException ..
     */
    public static void main ( String[] args ) throws IOException {
        byte[] file_contents = Files.readAllBytes ( Paths.get ( System.getProperty ( "user.dir" ) + "\\src\\pack\\alben.xml" ) );
        try {
            ArrayList < String > tokenlist = createTokenList ( file_contents, 0 );
            ArrayList < Album > ret = parseFile ( tokenlist );
            System.out.println ( ret.toString () );
        } catch ( Exception e ) {
            System.err.print ( e.getMessage () );
        }
    }

    /**
     * createTokenList: creates a token list from xml document
     * <ul>
     *     <li>Loop through xml file via recursion</li>
     *     <li>Match each line against regular expression</li>
     *     <li>Filter out xml syntax</li>
     *     <li>Write raw content into ArrayList</li>
     * </ul>
     * @param fileContent the xml file to parse
     * @return ArrayList containing all tokens
     * @throws Exception ..
     */
    public static ArrayList < String > createTokenList ( byte[] fileContent, int index ) throws Exception {
        ArrayList < String > tokenlist = new ArrayList <> ();
        try {
            if(fileContent[index] == '\n' || fileContent[index] == '\r' || fileContent[index] == '\t')
                return createTokenList(fileContent, index + 1);
            else if(fileContent[index] == '>') {
                String token = getRawData(fileContent, index + 1);
                tokenlist.add(token);
            } else if (fileContent[index] == '<') {
                String token = getRawData(fileContent, index + 1);
                tokenlist.add(token);
            }
            /*Pattern pattern = Pattern.compile ( "<[a-z]+?>|</[a-z]+?>|>[a-zA-Z 0-9.:()’]+?<" );
            Matcher matcher = pattern.matcher ( new String ( fileContent , StandardCharsets.UTF_8 ) );

            if ( matcher.find () ) {
                tokenlist.add ( matcher.group ().substring ( 1 , matcher.end () - 1 - matcher.start () ) );
            }
            tokenlist.addAll ( createTokenList ( Arrays.copyOfRange ( fileContent , matcher.end () - 1 , fileContent.length ) ) );*/
            tokenlist.addAll(createTokenList(fileContent,index + 1));

            return tokenlist;
        } catch ( Exception e ) {
            return tokenlist;
        }
    }

    public static String getRawData (byte[] fileContent, int index) {
        if(fileContent[index] == '<' || fileContent[index] == '>')
            return "";
        return  (char)fileContent[index] + getRawData(fileContent, index + 1);
    }

    /**
     * parseFile: parses all albums
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Write all albums with respective information into ArrayList</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return an ArrayList containing all albums with respective information (ie title, artist)
     * @throws Exception ..
     */
    public static ArrayList < Album > parseFile ( ArrayList < String > tokenList ) throws Exception {
        if ( tokenList.isEmpty () ) return new ArrayList <> ();
        Pattern pattern = Pattern.compile ( "album" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {
            Album album = new Album ();
            album.title = parseAlbumTitle ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            album.date = parseAlbumDate ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            album.artist = parseAlbumArtist ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            album.tracks = parseAlbumTracks ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );

            ArrayList < Album > ret = parseFile ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            ret.add ( 0 , album );
            return ret;
        } else return parseFile ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }

    /**
     * parseAlbumTracks: similar to parseFile, but parses all tracks of an album
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Write all album tracks with respective information into ArrayList</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return an ArrayList containing all tracks with respective information (ie title, length)
     */
    public static ArrayList < Track > parseAlbumTracks ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () || tokenList.get ( 0 ).equals ( "/album" ) )
            return new ArrayList <> ();    // Rekursionsschluss


        Pattern pattern = Pattern.compile ( "track" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {
            Track track = new Track ();
            track.title = parseTrackTitle ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            track.length = parseTrackLength ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            track.rating = Integer.parseInt ( parseTrackRating ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) ) );
            track.features = parseTrackFeature ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            track.writers = parseTrackWriter ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            ArrayList < Track > ret = parseAlbumTracks ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            ret.add ( 0 , track );
            return ret;

        } else return parseAlbumTracks ( new ArrayList < String > ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }

    /**
     * parseAlbumTitle: get the title of the current album
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Skip forward if a track is found to distinguish between track and album title</li>
     *     <li>Return respective album title</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return the respective album title
     */
    public static String parseAlbumTitle ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () ) return "";

        Pattern pattern = Pattern.compile ( "track" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {
            return parseAlbumTitle ( skipTrack ( tokenList ) );
        } else {
            pattern = Pattern.compile ( "title" );
            matcher = pattern.matcher ( tokenList.get ( 0 ) );

            if ( matcher.matches () ) {
                return tokenList.get ( 1 );
            } else return parseAlbumTitle ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
        }
    }

    /**
     * skipTrack: skip forward if a track is found to distinguish between track and album title
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Return token list after skipping forward</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return the altered token list where a track and its respective information have been removed
     */
    public static ArrayList < String > skipTrack ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () ) return tokenList;

        Pattern pattern = Pattern.compile ( "/track" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {
            return new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) );
        } else return skipTrack ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }

    /**
     * parseAlbumArtist: get the artist of the current album
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Return respective album artist</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return the respective album artist
     */
    public static String parseAlbumArtist ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () ) return "";
        Pattern pattern = Pattern.compile ( "artist" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {
            return tokenList.get ( 1 );
        } else return parseAlbumArtist ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }

    /**
     * parseAlbumDate: get the date of the current album
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Return the respective album date</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return the respective album date
     */
    public static String parseAlbumDate ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () ) return "";
        Pattern pattern = Pattern.compile ( "date" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {
            return tokenList.get ( 1 );
        } else return parseAlbumDate ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }

    /**
     * parseTrackTitle: get the title of the current track
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Return the respective track title</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return the respective track title
     */
    public static String parseTrackTitle ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () || tokenList.get ( 0 ).equals ( "/track" ) ) return "null";

        Pattern pattern = Pattern.compile ( "title" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {
            return tokenList.get ( 1 );
        } else return parseTrackTitle ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }

    /**
     * parseTrackLength: get the length of the current track
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Return the respective track length or null if no length is specified</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return the respective track length
     */
    public static String parseTrackLength ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () || tokenList.get ( 0 ).equals ( "/track" ) ) return "null";

        Pattern pattern = Pattern.compile ( "length" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {
            return tokenList.get ( 1 );
        } else return parseTrackLength ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }

    /**
     * parseTrack: get the rating of the current track
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Return the respective track rating or 0 if no rating is specified</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return the respective track rating
     */
    public static String parseTrackRating ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () || tokenList.get ( 0 ).equals ( "/track" ) ) return "0";

        Pattern pattern = Pattern.compile ( "rating" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {
            return tokenList.get ( 1 );
        } else return parseTrackRating ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }

    /**
     * parseTrackFeature: get the feature(s) of the current track
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Return the respective track feature. Return an empty ArrayList if no feature is specified</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return an ArrayList containing the respective track feature(s)
     */
    public static ArrayList < String > parseTrackFeature ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () || tokenList.get ( 0 ).equals ( "/track" ) ) return new ArrayList <  > ();

        Pattern pattern = Pattern.compile ( "feature" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {

            ArrayList < String > ret = parseTrackFeature ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            ret.add ( 0 , tokenList.get ( 1 ) );
            return ret;

        } else return parseTrackFeature ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }

    /**
     * parseTrackWriter: get the writer(s) of the current track
     * <ul>
     *     <li>Loop through token list via recursion</li>
     *     <li>Match each entry against regular expression</li>
     *     <li>Return the respective track writer. Return an empty ArrayList if no feature is specified.</li>
     * </ul>
     * @param tokenList the token list to operate on
     * @return an ArrayList containing the respective track writer(s)
     */
    public static ArrayList < String > parseTrackWriter ( ArrayList < String > tokenList ) {
        if ( tokenList.isEmpty () || tokenList.get ( 0 ).equals ( "/track" ) ) return new ArrayList < String > ();

        Pattern pattern = Pattern.compile ( "writing" );
        Matcher matcher = pattern.matcher ( tokenList.get ( 0 ) );

        if ( matcher.matches () ) {

            ArrayList < String > ret = parseTrackWriter ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
            ret.add ( 0 , tokenList.get ( 1 ) );
            return ret;
        } else return parseTrackWriter ( new ArrayList <> ( tokenList.subList ( 1 , tokenList.size () ) ) );
    }
}
